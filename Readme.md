# Welcome to michefood 👋
![Version](https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000)

> React native app to add and see friendly places to lunch in Colima.

## Getting started

Before start installing anything, make sure you have already installed Xcode version 10.x.x (to avoid some react native issues)

So, you need to configure ...
```sh
Xcode Command Line Tools

You will also need to install the Xcode Command Line Tools. Open Xcode, then choose 'Preferences...'
from the Xcode menu. Go to the Locations panel and install the tools by selecting the most recent
version in the Command Line Tools dropdown.
```

### Make sure you have already installed this dependencies

```sh
yarn
node
watchman
AdoptOpenJDK/openjdk
adoptopenjdk8
react-native-cli
```

if you don't have one of this already installed you can do it in this way

```sh
brew install yarn
brew install node
brew install watchman
brew tap AdoptOpenJDK/openjdk
brew cask install adoptopenjdk8
npm install -g react-native-cli
```

to see more https://facebook.github.io/react-native/docs/getting-started

## Usage

```sh
yarn && react-native run-ios
```

## Run tests

```sh
npm run test
```

## Show your support

Give a ⭐️ if this project helped you!

